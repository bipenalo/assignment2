# README #

* Code to train the LeNet5 architecture on the CIFAR10 dataset.
* Code to train LSTM/GRU/Basic RNN on the legendary MNIST dataset.
* Code to generate visualizations for the weights and activations. 

### How do I get set up? ###

The are two .npy files that include the weights of the first convolutional layer for the trained LeNet as well as the activations for the last batch of the test set.
