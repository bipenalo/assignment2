from scipy import misc
import numpy as np
import tensorflow as tf
import os
import time
import random
import matplotlib.pyplot as plt
import matplotlib as mp

# --------------------------------------------------
# setup

def weight_variable(name, shape):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''

    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE
    W = tf.get_variable(name, shape, initializer=tf.contrib.layers.xavier_initializer())
    return W

def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''

    # IMPLEMENT YOUR BIAS_VARIABLE HERE
    initial = tf.constant(0.1, shape=shape)
    b = tf.Variable(initial)

    return b

def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''

    # IMPLEMENT YOUR CONV2D HERE
    h_conv = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

    return h_conv

def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''

    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    h_max = tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding= 'SAME')
    return h_max

# Defines the chosen Nonlinearity
def NonLinearity(z, type):

    return{
            'Tanh': tf.tanh(z),
            'Sigmoid': tf.sigmoid(z),
            'ReLU': tf.nn.relu(z),
            'Elu': tf.nn.elu(z),
            }[type]

def optimizer_type(lr, type):

    return{
            'GDO': tf.train.GradientDescentOptimizer(lr),
            'Adam': tf.train.AdamOptimizer(lr),
            'Adagrad': tf.train.AdagradOptimizer(lr),

            }[type]

# Function defined to generate first layer's weights visualization.
def plot_weights(W):
    '''
    :param W: Tensor with filters of the first convolutional layer
    :return: Visualitation
    '''
    W = np.reshape(W, [W.shape[0], W.shape[1], 1, W.shape[2]*W.shape[3]])
    fig, ax = plt.subplots(5, 7, squeeze=True)
    j = 0
    k = 0
    #plt.figure()

    for i in range(W.shape[3]):
        if (i%7 == 0) & (i > 1):
            j += 1
            k = 0
        ax[j,k].axis('off')
        ax[j, k].imshow(np.squeeze(W[:, :, :, i]), interpolation='nearest', cmap='Greys_r')
        k += 1
    plt.savefig('test1.png', bbox_inches ='tight')
    plt.show()

def variable_summaries(var, name):
    """Attach a lot of summaries to a Tensor."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.scalar_summary('mean/' + name, mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
    tf.scalar_summary('stddev/' + name, stddev)
    tf.scalar_summary('max/' + name, tf.reduce_max(var))
    tf.scalar_summary('min/' + name, tf.reduce_min(var))

    tf.histogram_summary(name, var)

# Generates a single histogram with the average activations over the batchsize and feature maps.
def calculate_statistics(h):
    f = plt.figure()
    ax = f.add_subplot(111)
    tot = np.zeros((28,28))

    for k in range(10):
        var = np.zeros((28,28))
        for i in range(32):
            var += h[k,:,:,i]/32 # 32 feature maps

        tot += var/10 # 10 batchsize
    ax.hist(tot, bins=100, range=(0,1))
    #plt.savefig('step%d' % k)


start_time = time.time()


ntrain = 100 # per class
ntest =  10 # per class
nclass =  10 # number of classes
imsize = 28
nchannels = 1
batchsize = 10

Train = np.zeros((ntrain*nclass,imsize,imsize,nchannels))
Test = np.zeros((ntest*nclass,imsize,imsize,nchannels))
LTrain = np.zeros((ntrain*nclass,nclass))
LTest = np.zeros((ntest*nclass,nclass))

itrain = -1
itest = -1
for iclass in range(0, nclass):
    for isample in range(0, ntrain):
        path = '/Users/Boris/PycharmProjects/Assignment2/CIFAR10/Train/%d/Image%05d.png' % (iclass,isample)
        im = misc.imread(path)  # 28 by 28
        im = im.astype(float)/255
        itrain += 1
        Train[itrain,:,:,0] = im
        LTrain[itrain,iclass] = 1 # 1-hot lable
    for isample in range(0, ntest):
        path = '/Users/Boris/PycharmProjects/Assignment2/CIFAR10/Test/%d/Image%05d.png' % (iclass, isample)
        im = misc.imread(path) # 28 by 28
        im = im.astype(float)/255
        itest += 1
        Test[itest,:,:,0] = im
        LTest[itest,iclass] = 1 # 1-hot lable

sess = tf.InteractiveSession()


# placeholders for input data and input labeles
with tf.name_scope('Input'):

    x = tf.placeholder(tf.float32, [None, 28, 28, 1]) #tf variable for the data, remember shape is [None, width, height, numberOfChannels]
    y_ = tf.placeholder(tf.float32, shape=[None, 10])#tf variable for labels

# --------------------------------------------------
# model
#create your model
ActFunc = 'ReLU'
optimizer_method = 'Adam'
learning_rate = 1e-5
result_dir = './histograms/' # directory where the results from the training are saved

start_time = time.time()

# first convolutional layer
with tf.name_scope('conv_layer1'):
    W_conv1 = weight_variable('conv1_weights' ,[5, 5, 1, 32])
    #variable_summaries(W_conv1, 'conv1_weights')
    b_conv1 = bias_variable([32])
    #variable_summaries(b_conv1, 'conv1_bias')
    pre_act1 = conv2d(x, W_conv1) + b_conv1
    #variable_summaries(pre_act1, 'pre_activations_layer1')
    h_conv1 = NonLinearity(pre_act1, ActFunc)
    variable_summaries(h_conv1, 'ReLU_act_layer1')
    h_pool1 = max_pool_2x2(h_conv1)
    #variable_summaries(h_pool1, 'Maxpool_act_layer1')

# second convolutional layer
with tf.name_scope('conv_layer2'):
    W_conv2 = weight_variable('conv2_weights', [5, 5, 32, 64])
    #variable_summaries(W_conv2, 'conv2_weights')
    b_conv2 = bias_variable([64])
    #variable_summaries(b_conv2, 'conv2_bias')
    pre_act2 = conv2d(h_pool1, W_conv2) + b_conv2
    #variable_summaries(pre_act2, 'pre_activations_layer2')
    h_conv2 = NonLinearity(pre_act2, ActFunc)
    #variable_summaries(h_conv2, 'ReLU_act_layer2')
    h_pool2 = max_pool_2x2(h_conv2)
    #variable_summaries(h_pool2, 'Maxpool_act_layer2')

# densely connected layer
with tf.name_scope('Fully_connected_layer_L1'):
    W_fc1 = weight_variable('FC_weights_L1', [7 * 7 * 64, 1024])
    #variable_summaries(W_fc1, 'FC_weights_L1')
    b_fc1 = bias_variable([1024])
    #variable_summaries(b_fc1, 'FC_bias_L1')
    h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
    pre_act_FC1 = tf.matmul(h_pool2_flat, W_fc1) + b_fc1
    #variable_summaries(pre_act_FC1, 'pre_activations_FC_L1')
    h_fc1 = NonLinearity(pre_act_FC1, ActFunc)
    #variable_summaries(h_fc1, 'ReLU_FC_L1')

# dropout
with tf.name_scope('Dropout'):
    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# Softmax Layer
with tf.name_scope('Fully_connected_layer_L2'):
    W_fc2 = weight_variable('FC_weights_L2', [1024, 10])
    #variable_summaries(W_fc2, 'FC_weights_L2')
    b_fc2 = bias_variable([10])
    #variable_summaries(b_fc2, 'FC_bias_L2')
    pre_act_FC2 = tf.matmul(h_fc1_drop, W_fc2) + b_fc2
    #variable_summaries(pre_act_FC2, 'pre_activations_FC_L2')
    # Select the Loss function
    y_conv = tf.nn.softmax(pre_act_FC2)

    #variable_summaries(y_conv, 'Network Output')

# --------------------------------------------------
# loss
#set up the loss, optimization, evaluation, and accuracy

with tf.name_scope('Loss_function'):
    Loss = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y_conv), reduction_indices=[1]))

    # Add a scalar summary for the snapshot loss.
    #tf.scalar_summary(Loss.op.name, Loss)

with tf.name_scope('train'):
    train_step = optimizer_type(learning_rate, optimizer_method).minimize(Loss)
with tf.name_scope('accuracy'):
    with tf.name_scope('correct_prediction'):
        correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
    with tf.name_scope('accuracy'):
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Build the summary operation based on the TF collection of Summaries.
summary_op1 = tf.merge_all_summaries()
# Create a saver for writing training checkpoints.
saver = tf.train.Saver()
# Instantiate a SummaryWriter to output summaries and the Graph.
summary_writer = tf.train.SummaryWriter(result_dir, sess.graph)

# --------------------------------------------------
# optimization
'''
g = tf.get_default_graph()
names = [op.name for op in g.get_operations()]
print(names)
'''

sess.run(tf.initialize_all_variables())
batch_xs = np.zeros((batchsize, 28, 28, 1)) #setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
batch_ys = np.zeros((batchsize, 10))#setup as [batchsize, the how many classes]
L = []
Training_acc = []
Test_acc = []
for i in range(10): # try a small iteration size once it works then continue
    perm = np.arange(ntrain*nclass)
    np.random.shuffle(perm)
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Train[perm[j],:,:,:]
        batch_ys[j,:] = LTrain[perm[j],:]
    if i%10 == 0:
        #calculate train accuracy and print it
        Training_acc.append(accuracy.eval(feed_dict={x: batch_xs, y_: batch_ys, keep_prob: 1.0})) # for real run, changed batch_xs to Train And batch_ys to LTrain
        L.append(Loss.eval(feed_dict={x: batch_xs, y_: batch_ys, keep_prob: 1.0}))
        Test_acc.append(accuracy.eval(feed_dict= {x:Test, y_:LTest, keep_prob: 1.0}))

        #get the tensor for the activations of convLayer 1
        activations = sess.run(h_conv1, feed_dict={x: batch_xs, y_: batch_ys, keep_prob: 1.0})
        calculate_statistics(activations)

        #create the histograms for convnet Layer 1
        summary_str1 = sess.run(summary_op1, feed_dict={x: batch_xs, y_: batch_ys, keep_prob: 1.0})
        summary_writer.add_summary(summary_str1, i)
        summary_writer.flush()

        print("train accuracy %g"%accuracy.eval(feed_dict={x: Train, y_: LTrain, keep_prob: 1.0}))

        checkpoint_file = os.path.join(result_dir, 'checkpoint')
        saver.save(sess, checkpoint_file, global_step=i)
    # Training step
    train_step.run(feed_dict= {x: batch_xs, y_: batch_ys, keep_prob: 1.0}) # dropout only during training

# --------------------------------------------------
# test




print("test accuracy %g"%accuracy.eval(feed_dict={x: Test, y_: LTest, keep_prob: 1.0}))

# Stores values for weights and activations to be used with the Plots.py file
WEIGHTS = sess.run(W_conv1)
activations = sess.run(h_conv1, feed_dict={x: batch_xs, y_: batch_ys, keep_prob: 1.0})
np.save('Weigths_firstLayer', WEIGHTS)
np.save('activations', activations)



#calculate_statistics(act1)

plot_weights(WEIGHTS)

stop_time = time.time()
print('The training takes %f second to finish'%(stop_time - start_time))

sess.close()