import numpy as np
import tensorflow as tf
import os
import time
import matplotlib.pyplot as plt
import matplotlib as mp

# Class to generate the visualizations for the activations
class graphs():


    def plot_weights(self, name, W):
        '''
        :param W: Tensor with filters of the first convolutional layer
        :return: Visualitation
        '''
        fig, ax = plt.subplots(5, 7, squeeze=True)
        j = 0
        k = 0

        for i in range(32):
            if (i%7 == 0) & (i > 1):
                j += 1
                k = 0
            if name == 'im':
                ax[j,k].axis('off')
                ax[j, k].imshow(np.squeeze(W[:, :, :, i]), interpolation='kaiser', cmap='Greys_r')
                k += 1

            if name == 'act1':
                ax[j,k].axis('off')

                # Uncomment if you want to visualize the histograms of the activations for each filter.
                #ax[j, k].hist(W[:,:,i], bins=100, range=(0,1))

                #visualize the activations as grayscale cells
                ax[j, k].imshow(np.squeeze(W[:, :, i]), interpolation='kaiser', cmap='Greys_r')
                ax[4, 5].axis('off')
                ax[4, 6].axis('off')
                k += 1
        ax[4,4].axis('off')
        ax[4,5].axis('off')
        ax[4,6].axis('off')
        plt.savefig('test_' + name + '.png', bbox_inches ='tight')
        plt.show()


    def calculate_statistics(self, h):
        #f = plt.figure()
        #ax = f.add_subplot(111)
        tot = np.zeros((28, 28, 32))

        for k in range(h.shape[0]):
            '''
            var = np.zeros((28,28))
            for i in range(32):
                var += h[k,:,:,i]/h.shape[3]

            tot += var/h.shape[0]
            '''
            tot += h[k,:,:,:]/h.shape[0]
        #self.plot_weights('act', tot)
        #ax.hist(tot, bins=100, range=(0,1))
        #plt.savefig('step%d' % k)
        return tot


def main():

    #load data .npy file with the first layer weight's of the network
    im = np.load('/Users/Boris/PycharmProjects/Assignment2/server_data1/test8/Weigths_firstLayer.npy')
    # load a .npy file with the activations values of the first layer.
    act = np.load('/Users/Boris/PycharmProjects/Assignment2/server_data1/test8/activations.npy')

    # reshapes the tensor so as to be used in the graph method.
    im = np.reshape(im, [im.shape[0], im.shape[1], 1, im.shape[2]*im.shape[3]])


    plots = graphs()
    # generate plots for the weights and activations
    activations = plots.calculate_statistics(act)

    plots.plot_weights('im', im)
    plots.plot_weights('act1', activations)

    # data to generate the plot error vs training size
    #error_training =[0, 0, 0.05, 0.089, 0.199, 0.199 ]
    #error_test =[0.75, 0.7, 0.59, 0.55, 0.5, 0.48]
    #m = [100, 1000, 2500, 5000, 7500, 10000]
    #plt.plot(m, error_training, m, error_test)

if __name__ == "__main__":
    main()
